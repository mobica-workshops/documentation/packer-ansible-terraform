#!/usr/bin/env bash
rm -Rf roles/common.*
mkdir -p roles/common.base
rsync -av --progress . roles/common.base --exclude roles